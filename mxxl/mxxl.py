import numpy as np


def read_snapshot(file_path: str) -> np.ndarray:
    """
    Read a Millennium XXL snapshot file (file_path) and returns the 3-D particle coordinates.

    The file contains two groups of data:
        - Number of particles stored in this file. The data type is little-endian 4-byte integer ('<i4')
        - particle_coord (N,3) -> 3-D coordinates of the particles. The coordinates of each particle are stored as
            three contiguous little-endian 4-byte floats ('<f4')

    :param file_path: Full path to the snapshot file, including the file name.
                      For example '/path/to/data/Snapshot_063/snap_mxxl.0'
    :return: (N,3) numpy array containing the particle coordinates
    """
    with open(file_path, 'rb') as file_in:
        # Skipping the first 4 bytes (offset), which contain the number of particles (32-bit int).
        particle_coord = np.fromfile(file_in, dtype='(3,)<f4', offset=4)

    return particle_coord


def read_fof_properties(file_path: str) -> tuple:
    """
    Read the file containing the friend-of-friend (FoF) group properties from a Millennium XXL file (file_path).

    The file contains several groups of data:
        - Number of groups stored in this file. The data type is little-endian 4-byte integer ('<i4')
        - group_len (N,) -> Array with the number of particles in each FoF group. The data type is little-endian 4-byte
            integer ('<i4')
        - group_offset (N,) -> Array with the index of the first particle (offset) in the particle list for each FoF group.
            The data type is little-endian 4-byte integer ('<i4')
        - group_cm (N,3) -> 3-D coordinates of center of mass of each FoF group. The coordinates are stored as three
            contiguous little-endian 4-byte floats ('<f4')
        - group_pos (N,3) -> 3-D coordinates of the particle with the lowest potential energy (?) for each FoF group.
            The coordinates of each particle are stored as three contiguous little-endian 4-byte floats ('<f4')
        - m_200 (N,) -> Array with the 200-rho_b mass of each FoF group. The data type is little-endian 4-byte float ('<f4')
        - m_cri (N,) -> Array with the 200-rho_cri mass of each FoF group. The data type is little-endian 4-byte float ('<f4')
        - m_vir (N,) -> Array with the virial mass of each FoF group. The data type is little-endian 4-byte float ('<f4')

    :param file_path: Full path to the snapshot file, including the file name.
                      For example '/path/to/data/Fof/halo.0'
    :return: (group_len, group_offset, group_cm, group_pos, m_200, m_cri, m_vir)
    """
    with open(file_path, 'rb') as file_in:
        n_groups = np.fromfile(file_in, dtype='<i4', count=1)[0]
        group_len = np.fromfile(file_in, dtype='<i4', count=n_groups)
        group_offset = np.fromfile(file_in, dtype='<i4', count=n_groups)
        group_cm = np.fromfile(file_in, dtype='(3,)<f4', count=n_groups)
        group_pos = np.fromfile(file_in, dtype='(3,)<f4', count=n_groups)
        m_200 = np.fromfile(file_in, dtype='<f4', count=n_groups)
        m_cri = np.fromfile(file_in, dtype='<f4', count=n_groups)
        m_vir = np.fromfile(file_in, dtype='<f4', count=n_groups)
    return group_len, group_offset, group_cm, group_pos, m_200, m_cri, m_vir


def read_fof_particles(file_path):
    """
    Read the file containing the coordinates of particles in the friend-of-friend (FoF) groups from a Millennium XXL
    file (file_path).

    The file contains several groups of data:
        - Number of particles stored in this file. The data type is little-endian 4-byte integer ('<i4')
        - p_mass -> The particle mass used in the simulation. The data type is little-endian 8-byte float ('<f8')
        - time -> The simulation time that the file corresponds to. The data type is little-endian 8-byte float ('<f8')
        - box_size -> The size of the simulated box in Mpc(?). The data type is little-endian 8-byte float ('<f8')
        - pos_list (N,3) -> 3-D coordinates of the particles belonging to each FoF group. The coordinates of each
            particle are stored as three contiguous little-endian 4-byte floats ('<f4')

    :param file_path: Full path to the snapshot file, including the file name.
                      For example '/path/to/data/Fof/halo_part.0'
    :return: (p_mass, time, box_size, pos_list)
    """
    with open(file_path, 'rb') as file_part:
        n_part, p_mass, time, box_size = np.fromfile(file_part, dtype='<i4, <f8, <f8, <f8', count=1)[0]
        pos_list = np.fromfile(file_part, dtype='(3,)<f4', count=n_part)
    return p_mass, time, box_size, pos_list


def read_full_catalogue(file_path):
    """
    Read a subfind(?) halo catalogue from a Millennium XXL file (file_path).

    The file contains several groups of data:
        - Number of haloes stored in this file. The data type is little-endian 4-byte integer ('<i4')
        - 3-D coordinates of the particle with the lowest potential energy (?) for each halo.
            The coordinates each particle are stored as three contiguous little-endian 4-byte floats ('<f4')
        - Array with the 200-rho_cri mass of each halo. The data type is little-endian 4-byte float ('<f4')
        - Array with the v_max of each halo. The data type is little-endian 4-byte float ('<f4')

    :param file_path: Full path to the snapshot file, including the file name.
                      For example '/path/to/data/Fof/vhalos_063_1e13.dat'
    :return: (N,5) numpy array containing the halo properties: halo centre, m_cri and v_max
    """
    with open(file_path, 'rb') as file_in:
        # Skipping the first 4 bytes (offset), which contain the number of particles (32-bit int).
        halo_cat = np.fromfile(file_in, dtype='(5,)<f4', offset=4)
    return halo_cat

