import numpy as np


class Subfind:

    def __init__(self, fname='subhalo_tab_063', i=0):
        self.fname = fname
        self.header(i)
        self.fin.close()

    def header(self, i=0):
        self.fin = open(self.fname + '.' + str(i), 'rb')

        self.Ng = np.fromfile(self.fin, dtype='i', count=1)[0]
        self.Ng_tot = np.fromfile(self.fin, dtype='l', count=1)[0]
        self.Nid = np.fromfile(self.fin, dtype='i', count=1)[0]
        self.Nid_tot = np.fromfile(self.fin, dtype='l', count=1)[0]
        self.Ntask = np.fromfile(self.fin, dtype='i', count=1)[0]
        self.Ns = np.fromfile(self.fin, dtype='i', count=1)[0]
        self.Ns_tot = np.fromfile(self.fin, dtype='l', count=1)[0]

    def load(self, i=0):
        self.header(i)

        # Group length
        self.glen = np.fromfile(self.fin, dtype='i', count=self.Ng)

        # Offset in id-list
        self.off = np.fromfile(self.fin, dtype='i', count=self.Ng)

        # Group number
        self.grnr = np.fromfile(self.fin, dtype='l', count=self.Ng)

        # CM
        self.cm = np.fromfile(self.fin, dtype='f', count=3 * self.Ng).reshape((self.Ng, 3))

        # Vel
        self.vel = np.fromfile(self.fin, dtype='f', count=3 * self.Ng).reshape((self.Ng, 3))

        # Potential minimum
        self.pos = np.fromfile(self.fin, dtype='f', count=3 * self.Ng).reshape((self.Ng, 3))

        # Mass : mean200, crit200, tophat200
        self.m200b = np.fromfile(self.fin, dtype='f', count=self.Ng)
        self.m200c = np.fromfile(self.fin, dtype='f', count=self.Ng)
        self.m200t = np.fromfile(self.fin, dtype='f', count=self.Ng)

        # Velocity dispersion : mean200, crit200, tophat200
        self.vdfof = np.fromfile(self.fin, dtype='f', count=self.Ng)
        self.vd200b = np.fromfile(self.fin, dtype='f', count=self.Ng)
        self.vd200c = np.fromfile(self.fin, dtype='f', count=self.Ng)
        self.vd200t = np.fromfile(self.fin, dtype='f', count=self.Ng)

        # Number of substructures in FoF group
        self.Nsubs = np.fromfile(self.fin, dtype='i', count=self.Ng)

        # First substructure in FOF group
        self.Fsub = np.fromfile(self.fin, dtype='i', count=self.Ng)

        # Length of substructure
        self.slen = np.fromfile(self.fin, dtype='i', count=self.Ns)

        # offset of substructure
        self.soff = np.fromfile(self.fin, dtype='i', count=self.Ns)

        # Group Number of substructure
        self.sgrnr = np.fromfile(self.fin, dtype='l', count=self.Ns)

        # Subgroup Number of substructure
        self.ssnr = np.fromfile(self.fin, dtype='l', count=self.Ns)

        # Potential minimum of substructure
        self.spos = np.fromfile(self.fin, dtype='f', count=3 * self.Ns).reshape((self.Ns, 3))

        # CM of substructure
        self.scm = np.fromfile(self.fin, dtype='f', count=3 * self.Ns).reshape((self.Ns, 3))

        # Vel of substructure
        self.svel = np.fromfile(self.fin, dtype='f', count=3 * self.Ns).reshape((self.Ns, 3))

        # Spin of substructure
        self.sspin = np.fromfile(self.fin, dtype='f', count=3 * self.Ns).reshape((self.Ns, 3))

        # Velocity dispersion of substructure
        self.svd = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Maximum circular velocity of substructure
        self.smcv = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Radius of maximum circular velocity of substructure
        self.srcv = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Radius of half mass of substructure
        self.srhm = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Shape of substructure
        self.sshape = np.fromfile(self.fin, dtype='f', count=6 * self.Ns).reshape((self.Ns, 6))

        # Binding energy of substructure
        self.sbe = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Potential energy of substructure
        self.spe = np.fromfile(self.fin, dtype='f', count=self.Ns)

        # Mass at different radii of substructure
        self.smprof = np.fromfile(self.fin, dtype='f', count=9 * self.Ns).reshape((self.Ns, 9))

        self.fin.close()

#        self.r200c = ( self.m200c / 200 * self.mxxl_path.cosmo.omegam( 1., self.mxxl_path.Omega0 ) / self.mxxl_path.rho_back / (4.*np.pi/3)  )**(1./3)
