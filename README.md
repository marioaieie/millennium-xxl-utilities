# Millennium XXL python utilities

A bunch of utilities to read and process the Millennium XXL simulation data.

## Usage

The usage is pretty straightforward. There is also a small notebook with usage example of the functions